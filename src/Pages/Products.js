// import productData from "../data/productData"
import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import ProductCard from '../Components/ProductCard';

export default function Products(){

    const [ products, setProducts ] = useState([])

    useEffect(() => {
        fetch('http://localhost:4000/products/')
        .then(res => res.json())
        .then(data => {
            console.log(data.length);

            // return (
            //     <div className='row'>
            //      {setProducts(data.map((product, index) => (
            //       <div className='col-md-4' key={index}>
            //          <ProductCard productProp = {product}/>
            //       </div>
            //      )))}
            //      "Some text value"
            //     </div>
            //  );

            setProducts(data.map(product => {
                return (
                    <div className='d-flex flex-row'>
                        <div>
                        <ProductCard key={product._id} productProp = {product}/>
                        </div>
                                  
                    </div>
                
                )
            }))
        })
    }, [])

        // const products = productData.map(product => {
        //     return (
        //         <ProductCard key={product.id} productProp = {product}/>
        //     )
        // })

    return(
        <>
        <div className='row'>
        <h1 className='products text-center p-5'>PRODUCTS</h1>
        {products}
        </div>
       
        </>
    )
}