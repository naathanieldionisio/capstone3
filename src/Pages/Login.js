import { useEffect, useState, useContext } from "react";
import { FormGroup, Form, Button, Card } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";


export default function Login() {

    const { user, setUser } = useContext(UserContext)

    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ isActive, setisActive ] = useState(false);
    console.log(email);
    console.log(password);

    function authenticate(e) {

        e.preventDefault();

        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })  
        .then(res => res.json())
        .then(data => {
            console.log(data)
            
            if (data.access !== undefined) {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                });
            } else {
                Swal.fire({
                    title: "Auth Failed",
                    icon: "error",
                    text: "Try Again"
                });
            }
        })
        
        // localStorage.setItem('email', email);

        // setUser({email: localStorage.getItem('email')})
        // console.log(email)

        setEmail('');
        setPassword('');
    
        // alert("You are now Login!")
    }

    const retrieveUserDetails = (token) => {

        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if(email !== '' && password !== ''){
            setisActive(true)
        } else {
            setisActive(false)
        }
    }, [email, password])

    return (
        (user.id !== null)
        ?
            <Navigate to='/'/>
        :
        <div className=" container-fluid row bglog">
            <div className="row col-sm-12 col-md-4 col-lg-3">
            
            </div>
                {/* //section------------- */}
            <div className="row col-sm-12 col-md-4 col-lg-6">
            <Card className="login" style={{ height: '44rem', width: '100%'}}> 
            <Card.Img variant="top" src="images/loginbg1.png" style={{ height: '14rem'}}/>
            <h1 className="text-center titlelog">Log in to your account</h1>
            <p className="text-center subT">Login using social networks</p>
            <div className="Col text-center">
            <img className='' src="/images/google.png" alt="" style={{ height: '30px', weight: '15px'}}/>
            <img className='icon' src="/images/facebook.png" alt="" style={{ height: '30px', weight: '15px'}}/>
            <img className='' src="/images/linkedin.png" alt="" style={{ height: '30px', weight: '15px'}}/>
            <p>──────────────────────  or  ───────────────────────</p>
            </div>
            <Card.Body>
            <Form onSubmit={(e) => authenticate(e)}>
            <FormGroup controlId="userEmail">
            <Form.Label className="emailForm">Email Address</Form.Label>
            <Form.Control
                type="email"
                placeholder="Enter Email"
                required
                value={email}
                onChange={e => setEmail(e.target.value)}
            />
            <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
            </FormGroup>

            <FormGroup controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password"
                    placeholder="Enter Password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </FormGroup>
                <br/>
                {isActive
                    ? 
                    <Button style={{ width: '100%'}} variant="primary" type="submit" id="submitBtn" >Submit</Button>
                    :
                    <Button style={{ width: '100%'}} variant="danger" type="submit" id="submitBtn" disabled>Submit</Button> 
                }
            </Form>
            </Card.Body>
            </Card>
            </div>
            <div className="row col-sm-12 col-md-4 col-lg-3">
            
            </div>
        </div>    
    )
}



