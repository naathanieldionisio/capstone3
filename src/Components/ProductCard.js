import { Link } from 'react-router-dom'
import { Card, Button, Container, Row } from 'react-bootstrap'
import CardGroup from 'react-bootstrap/CardGroup';

export default function ProductCard({productProp}) {
    const { name, description, price, _id } = productProp

    return (
    <Container className='text-center'>
        <div className='fix'>
        <div className="d-flex justify-content-around text-start">
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/target1.png" />
                <Card.Body>
                <Card.Title>Target Male Shirt</Card.Title>
                <Card.Text>
                Cuban Short Sleeve Shirt Regular Fit  <br/><br/>
                    PHP 1,599.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card>
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/brown1.png" />
                <Card.Body>
                <Card.Title>OXGN female Shirt</Card.Title>
                <Card.Text>
                Textured Slim Regular Fit T-Shirt <br/><br/>
                Php 499.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card>
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/red1.png" />
                <Card.Body>
                <Card.Title>OXGN female Shirt</Card.Title>
                <Card.Text>
                Textured Slim Regular Fit T-Shirt <br/><br/>
                Php 499.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card>   
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/target1.png" />
                <Card.Body>
                <Card.Title>Target Male Shirt</Card.Title>
                <Card.Text>
                    Frappe Embroidery Regular Fit T-Shirt <br/><br/>
                    PHP 599.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card>
            </div>
            {/* section --------------------------------------------------------------------------? */}
            <div className="d-flex justify-content-around text-start">
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/polo1.png" />
                <Card.Body>
                <Card.Title>H&M</Card.Title>
                <Card.Text>
                Relaxed Fit Short-Sleeved Shirt with Iconic Design <br/><br/>
                Php 999.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card> 
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/stripes1.png" />
                <Card.Body>
                <Card.Title>Cotton On</Card.Title>
                <Card.Text>
                Cuban stripes Short Sleeve Shirt <br/><br/>
                Php 1,599.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card> 
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/red1.png" />
                <Card.Body>
                <Card.Title>OXGN female Shirt</Card.Title>
                <Card.Text>
                Textured Slim Regular Fit T-Shirt <br/><br/>
                Php 499.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card>  
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/stripes1.png" />
                <Card.Body>
                <Card.Title>Cotton On</Card.Title>
                <Card.Text>
                Cuban stripes Short Sleeve Shirt <br/><br/>
                Php 1,599.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card> 
            </div>
            <div className="d-flex justify-content-around text-start">
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/polo1.png" />
                <Card.Body>
                <Card.Title>H&M</Card.Title>
                <Card.Text>
                Relaxed Fit Short-Sleeved Shirt with Iconic Design <br/><br/>
                Php 999.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card> 
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/stripes1.png" />
                <Card.Body>
                <Card.Title>Cotton On</Card.Title>
                <Card.Text>
                Cuban stripes Short Sleeve Shirt <br/><br/>
                Php 1,599.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card> 
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/red1.png" />
                <Card.Body>
                <Card.Title>OXGN female Shirt</Card.Title>
                <Card.Text>
                Textured Slim Regular Fit T-Shirt <br/><br/>
                Php 499.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card>  
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/stripes1.png" />
                <Card.Body>
                <Card.Title>Cotton On</Card.Title>
                <Card.Text>
                Cuban stripes Short Sleeve Shirt <br/><br/>
                Php 1,599.00
                </Card.Text>
                </Card.Body>
                <Button variant="dark">Details</Button>
            </Card> 
            </div>

        <CardGroup>
        <Card style={{ width: '100rem' }}>
            <Card.Img className="w-100" src="images/.png" alt=""/>   
            <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Button as={Link} variant="warning" to={`/products/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
            </CardGroup>
            </div>
    </Container>  
    )
}