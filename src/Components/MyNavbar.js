import { useContext } from 'react'
import { Navbar, Container, Nav } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import UserContext from '../UserContext';
import React from 'react';

export default function MyNavbar(){

    const { user } = useContext(UserContext);
    console.log(user)
    // const [user, setUser] = useState(localStorage.getItem('email'));
    

    return (
        <Navbar className='nav' expand="lg">
        <Container>
            <Navbar.Brand as={Link} to="/">
                <img className='logo' src="/images/logo.png" alt=""/>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto Navs">
                <Nav.Link className='homenav' as={NavLink} to="/">Home</Nav.Link>

                {
                    (user.isAdmin)
                    ?
                      <Nav.Link as={NavLink} to="/admin">AdminDashboard</Nav.Link>
                    :
                      <Nav.Link as={NavLink} to="/products">Shop</Nav.Link>

                }
                        <Nav.Link as={NavLink} to="/about">About</Nav.Link>
                        <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
                {(user.id !== null)
                    ? 
                     <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                    :
                    <>
                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                    </>
                }
            </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
    )
}