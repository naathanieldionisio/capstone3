// import productData from "../data/productData"
import { useEffect, useState } from 'react';
import CartCard from '../Components/CartCard';

export default function Cart(){
    const [ cart, setCarts ] = useState([])

    useEffect(() => {
        fetch('http://localhost:4000/cart', {
            method: "GET",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
        })
        .then(res => res.json())
        .then(data => {
            setCarts(data.map((order, index) => {
                return (
                    <div className='d-flex flex-row'>
                        <div>
                        <CartCard key={index} cartProp = {order}/>
                        </div>
                                  
                    </div>
                
                )
            }))
                
            

        })
    }, [])

        // const products = productData.map(product => {
        //     return (
        //         <ProductCard key={product.id} productProp = {product}/>
        //     )
        // })

    return(
        <>
        <div className='row'>
        <h1>Cart</h1>
        {cart}
        </div>
       
        </>
    )
}