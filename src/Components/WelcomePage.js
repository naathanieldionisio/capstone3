import { Row, Col, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';

export default function WelcomePage({data}) {

    const { title, content, destination, label } = data;

    return (
        <Row>
            <Col className='text-center p-5'>
                <h1>{title}</h1>
                <p>{content}</p>
                <Button as={Link} to={destination} variant="warning">{label}</Button>
            </Col>
        </Row>
    )
}