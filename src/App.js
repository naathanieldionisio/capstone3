import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './Pages/Home';
import MyNavbar from './Components/MyNavbar';
import Products from './Pages/Products';
// import { Container } from 'react-bootstrap';
import Register from './Pages/Register';
import Login from './Pages/Login';
import Cart from './Pages/Cart';
import Logout from './Pages/Logout';
import Error from './Pages/Error';
import { useEffect, useState } from 'react';
import { UserProvider } from './UserContext';
import ProductsView from './Pages/ProductsView';
import AdminDashboard from './Pages/adminDashboard'


function App() {

  const [ user, setUser] = useState({
      id: null,
      isAdmin: null
  });

  const unsetUser = () => {
      localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return(
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <MyNavbar/>
          <Routes>
             <Route path="/" element={<Home/>}/>
             <Route path="/products" element={<Products/>}/>
             <Route path="/cart" element={<Cart/>}/>
             <Route path="/products/:productId" element={<ProductsView/>}/>
             <Route path="/login" element={<Login/>}/>
             <Route path="/register" element={<Register/>}/>
             <Route path="/logout" element={<Logout/>}/>
             <Route path='*' element={<Error/>}/>
             <Route path="/admin" element={<AdminDashboard/>}/>
          </Routes>
      </Router>
    </UserProvider>

  );
}

export default App;
